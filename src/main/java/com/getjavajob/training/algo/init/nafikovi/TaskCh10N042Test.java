package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N042.raiseToPower;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N042Test {
    public static void main(String[] args) {
        testRaiseToPower1();
        testRaiseToPower2();
    }

    public static void testRaiseToPower1() {
        assertEquals("TaskCh10N042Test.testRaiseToPower1", 1, raiseToPower(1, 10));
    }

    public static void testRaiseToPower2() {
        assertEquals("TaskCh10N042Test.testRaiseToPower2", 32, raiseToPower(2, 5));
    }
}
