package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N015.charAtWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testCharAtWord();
    }

    public static void testCharAtWord() {
        assertEquals("TaskCh09N015Test.testCharAtWord", 't', charAtWord("test", 1));
    }
}
