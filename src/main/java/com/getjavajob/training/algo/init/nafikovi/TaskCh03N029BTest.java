package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029B.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029BTest {
    public static void main(String[] args) {
        testOneOfTwoLessThan20v1();
        testOneOfTwoLessThan20v3();
        testOneOfTwoLessThan20v2();
    }

    public static void testOneOfTwoLessThan20v1() {
        assertEquals("TaskCh03N029BTest.testOneOfTwoLessThan20v1", true, isTrue(15, 25));
    }

    public static void testOneOfTwoLessThan20v2() {
        assertEquals("TaskCh03N029BTest.testOneOfTwoLessThan20v2", false, isTrue(20, 21));
    }

    public static void testOneOfTwoLessThan20v3() {
        assertEquals("TaskCh03N029BTest.testOneOfTwoLessThan20v3", false, isTrue(10, 15));
    }
}
