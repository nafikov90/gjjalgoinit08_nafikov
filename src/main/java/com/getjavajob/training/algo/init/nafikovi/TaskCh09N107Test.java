package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N107.replaceLetters;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testReplace1();
        testReplace2();
    }

    public static void testReplace1() {
        assertEquals("TaskCh09N107Test.testReplace1", "ramo", replaceLetters("roma"));
    }

    public static void testReplace2() {
        assertEquals("TaskCh09N107Test.testReplace2", "these letters are missing", replaceLetters("test"));
    }
}
