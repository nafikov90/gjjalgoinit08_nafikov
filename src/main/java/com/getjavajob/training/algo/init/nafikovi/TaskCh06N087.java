package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh06N087.*;

public class TaskCh06N087 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the name 1 team");
        String team1 = scanner.nextLine();
        System.out.println("Enter the name 2 team");
        String team2 = scanner.nextLine();
        Game game = new Game(team1, team2);
        game.play();
    }
}

class Game {
    private String team1;
    private String team2;
    private int score1;
    private int score2;

    public Game(String team1, String team2) {
        this.team1 = team1;
        this.team2 = team2;
    }

    public void play() {
        System.out.println("Game started!!!");
        System.out.println(score());
        while (true) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int changeTeam = scanner.nextInt();
            if (changeTeam == 0) {
                break;
            }
            System.out.println("Enter score (1 or 2 or 3):");
            int scoreCurrent = scanner.nextInt();
            changeScore(changeTeam, scoreCurrent);
            System.out.println(score());
        }
        System.out.println("End of the game");
        System.out.println(result());
    }

    public String score() {
        return team1 + " " + score1 + " : " + score2 + " " + team2;
    }

    public void changeScore(int changeTeam, int scoreCurrent) {
        if (scoreCurrent > 3 || scoreCurrent < 1) {
            System.out.println("error input");
            return;
        } else if (changeTeam == 1) {
            score1 += scoreCurrent;
        } else if (changeTeam == 2) {
            score2 += scoreCurrent;
        } else {
            System.out.println("error input");
        }
    }

    public String findWinner() {
        if (score1 == score2) {
            return "Draw. ";
        } else {
            return score1 > score2 ? team1 + " is winner. " : team2 + " is winner. ";
        }
    }

    public String result() {
        return findWinner() + score();
    }
}
