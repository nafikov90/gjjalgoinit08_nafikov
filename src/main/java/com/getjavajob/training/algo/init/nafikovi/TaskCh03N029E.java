package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh03N029E {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int x = scanner.nextInt();
        System.out.println("Enter the number");
        int y = scanner.nextInt();
        System.out.println("Enter the number");
        int z = scanner.nextInt();
        boolean result = isTrue(x, y, z);
        System.out.println(result);
    }

    public static boolean isTrue(int x, int y, int z) {//return true if only one number is a multiple of 5
        boolean xx = x % 5 == 0;
        boolean yy = y % 5 == 0;
        boolean zz = z % 5 == 0;
        boolean res1 = xx ^ yy;
        return xx && yy ? false : res1 ^ zz;
    }
}
