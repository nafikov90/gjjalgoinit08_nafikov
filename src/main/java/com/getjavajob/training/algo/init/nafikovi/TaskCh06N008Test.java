package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh06N008.input;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testArrays();
    }

    public static void testArrays() {
        int[] testArray = {1, 4, 9};
        assertEquals("TaskCh06N008Test.testArrays", testArray, input(10));
    }
}
