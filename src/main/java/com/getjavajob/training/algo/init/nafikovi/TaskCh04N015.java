package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month of Birthday");
        int monthBirthday = scanner.nextInt();
        System.out.println("Enter the year of Birthday");
        int yearBirthday = scanner.nextInt();
        System.out.println("Enter the current month");
        int monthCurrent = scanner.nextInt();
        System.out.println("Enter the current year");
        int yearCurrent = scanner.nextInt();
        int result = calculateAge(monthBirthday, yearBirthday, monthCurrent, yearCurrent);
        System.out.println(result);
    }

    public static int calculateAge(int monthBirthday, int yearBirthday, int monthCurrent, int yearCurrent) {
        int age = yearCurrent - yearBirthday;
        return monthCurrent >= monthBirthday ? age : age - 1;
    }
}
