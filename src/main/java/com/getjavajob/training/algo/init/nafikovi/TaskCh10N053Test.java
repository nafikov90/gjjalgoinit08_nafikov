package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N053.ZERO_INDEX;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N053.reverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testReverseArr1();
        testReverseArr2();
        testReverseArr3();
    }

    public static void testReverseArr1() {
        int[] testArr = {5, 4, 3, 2, 1};
        int[] testResultArr = {1, 2, 3, 4, 5};
        assertEquals("TaskCh10N053Test.testReverseArr1",
                testResultArr, reverseArray(testArr, ZERO_INDEX, testArr.length - 1));
    }

    public static void testReverseArr2() {
        int[] testArr = {0, 0, 1};
        int[] testResultArr = {1, 0, 0};
        assertEquals("TaskCh10N053Test.testReverseArr2",
                testResultArr, reverseArray(testArr, ZERO_INDEX, testArr.length - 1));
    }

    public static void testReverseArr3() {
        int[] testArr = {0};
        int[] testResultArr = {0};
        assertEquals("TaskCh10N053Test.testReverseArr3",
                testResultArr, reverseArray(testArr, ZERO_INDEX, testArr.length - 1));
    }
}
