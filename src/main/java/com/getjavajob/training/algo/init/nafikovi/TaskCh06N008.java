package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number N");
        int[] result = input(scanner.nextInt());
        printResult(result);
    }

    public static int[] input(int n) {
        int numberOfElements = (int) Math.floor(Math.sqrt(n));
        int[] result = new int[numberOfElements];
        for (int i = 0; i < numberOfElements; i++) {
            result[i] = (int) Math.pow((i + 1), 2);
        }
        return result;
    }

    public static void printResult(int[] result) {
        for (int a : result) {
            System.out.println(a);
        }
    }
}
