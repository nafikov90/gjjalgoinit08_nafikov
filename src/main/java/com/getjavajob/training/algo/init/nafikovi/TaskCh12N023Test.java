package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testA();
        testB();
        testC();
    }

    public static void testA() {
        int[][] testArray = new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testA", testArray, fillTheArrayA(7));
    }

    public static void testB() {
        int[][] testArray = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("TaskCh12N023Test.testB", testArray, fillTheArrayB(7));
    }

    public static void testC() {
        int[][] testArray = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("TaskCh12N023Test.testC", testArray, fillTheArrayC(7));
    }
}
