package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        testIntermediateScore();
        testResult();
    }

    public static void testIntermediateScore() {
        Game testGame = new Game("A", "B");
        testGame.changeScore(1, 2);
        String trueScore = "A 2 : 0 B";
        assertEquals("TaskCh06N087Test.testIntermediateScore", trueScore, testGame.score());
    }

    public static void testResult() {
        Game testGame = new Game("A", "B");
        testGame.changeScore(1, 3);
        testGame.changeScore(2, 1);
        testGame.changeScore(1, 2);
        testGame.changeScore(2, 2);
        String expectResult = "A is winner. A 5 : 3 B";
        assertEquals("TaskCh06N087Test.testResult", expectResult, testGame.result());
    }
}
