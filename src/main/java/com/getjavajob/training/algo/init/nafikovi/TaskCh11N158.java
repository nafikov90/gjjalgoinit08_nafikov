package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of elements in the array");
        int[] input = new int[scanner.nextInt()];
        System.out.println("Enter the element values");
        for (int i = 0; i < input.length; i++) {
            input[i] = scanner.nextInt();
        }
        int[] result = deleteIdenticalElement(input);
        printArr(result);
    }

    public static int[] deleteIdenticalElement(int[] input) {
        int countOfDeletedElements = 0;
        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
                if (input[i] == input[j]) {
                    deleteElement(input, j);
                    countOfDeletedElements++;
                    if (i + countOfDeletedElements == input.length - 1) {
                        return input;
                    }
                }
            }
        }
        return input;
    }

    public static int[] deleteElement(int[] input, int num) {
        if (num == input.length - 1) {
            input[num] = 0;
            return input;
        }
        for (int i = num; i < input.length - 1; i++) {
            int tmp = input[i];
            input[i] = input[i + 1];
            input[i + 1] = tmp;
            if (i == input.length - 2) {
                input[i + 1] = 0;
            }
        }
        return input;
    }

    static void printArr(int[] input) {
        for (int x : input) {
            System.out.print(x + " ");
        }
        System.out.println();
    }
}
