package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N046.findMemberN;
import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N046.sumProgreesionMembers;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        testMemberN1();
        testMemberN2();
        testSum1();
        testSum2();
    }

    public static void testMemberN1() {
        assertEquals("TaskCh10N046Test.testMemberN1", 1, findMemberN(1, 10, 1));
    }

    public static void testMemberN2() {
        assertEquals("TaskCh10N046Test.testMemberN2", 4, findMemberN(1, 2, 3));
    }

    public static void testSum1() {
        assertEquals("TaskCh10N046Test.testSum1", 1, sumProgreesionMembers(1, 10, 1));
    }

    public static void testSum2() {
        assertEquals("TaskCh10N046Test.testSum2", 7, sumProgreesionMembers(1, 2, 3));
    }
}
