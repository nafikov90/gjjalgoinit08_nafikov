package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number system (2 <= N <= 16)");
        int nSystem = scanner.nextInt();
        System.out.println("Enter the natural number");
        int number = scanner.nextInt();
        int result = convertToAnotherSystem(number, nSystem);
        System.out.println(result);
    }

    public static int convertToAnotherSystem(int number, int nSystem) {
        int ratio = number / nSystem;
        int remainder = number % nSystem;
        if (ratio == 0) {
            return Integer.parseInt(Integer.toString(remainder));
        }
        return Integer.parseInt(convertToAnotherSystem(ratio, nSystem) + Integer.toHexString(remainder));
    }
}
