package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh09N042Test.testReverse", "rewoP", reverseWord("Power"));
    }
}
