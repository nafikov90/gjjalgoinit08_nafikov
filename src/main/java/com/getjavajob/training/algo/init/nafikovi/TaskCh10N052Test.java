package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N052.reverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverse1();
        testReverse2();
        testReverse3();
    }

    public static void testReverse1() {
        assertEquals("TaskCh10N052Test.testReverse1", 12, reverse(21));
    }

    public static void testReverse2() {
        assertEquals("TaskCh10N052Test.testReverse2", 1, reverse(100));
    }

    public static void testReverse3() {
        assertEquals("TaskCh10N052Test.testReverse3", 10009, reverse(90001));
    }
}
