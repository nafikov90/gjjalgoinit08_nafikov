package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int number = scanner.nextInt();
        outputNumber(number);
    }

    public static void outputNumber(int number) {
        System.out.println("You enter the number: " + number);
    }
}
