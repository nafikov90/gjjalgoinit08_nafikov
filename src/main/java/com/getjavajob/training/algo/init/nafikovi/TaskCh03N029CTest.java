package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029C.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029CTest {
    public static void main(String[] args) {
        testIsTrue1();
        testIsTrue2();
        testIsTrue3();
    }

    public static void testIsTrue1() {
        assertEquals("TaskCh03N029CTest.testIsTrue1", true, isTrue(0, 5));
    }

    public static void testIsTrue2() {
        assertEquals("TaskCh03N029CTest.testIsTrue2", true, isTrue(0, 0));
    }

    public static void testIsTrue3() {
        assertEquals("TaskCh03N029CTest.testIsTrue3", false, isTrue(2, 1));
    }
}
