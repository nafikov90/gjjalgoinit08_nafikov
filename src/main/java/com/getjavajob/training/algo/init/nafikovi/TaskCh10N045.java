package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first member of the progression");
        int firstMember = scanner.nextInt();
        System.out.println("Enter the difference of the progression");
        int difference = scanner.nextInt();
        System.out.println("Enter progression member number");
        int n = scanner.nextInt();
        int memberNumberN = findMemberN(firstMember, difference, n);
        System.out.println(n + " progression number: " + memberNumberN);
        int sumNMembers = sumProgreesionMembers(firstMember, difference, n);
        System.out.println("The sum of the first " + n + " members: " + sumNMembers);
    }

    public static int findMemberN(int firstMember, int difference, int n) {
        if (n == 1) {
            return firstMember;
        }
        return difference + findMemberN(firstMember, difference, n - 1);
    }

    public static int sumProgreesionMembers(int firstMember, int difference, int n) {
        if (n == 1) {
            return firstMember;
        }
        return firstMember + sumProgreesionMembers(firstMember + difference, difference, n - 1);
    }
}
