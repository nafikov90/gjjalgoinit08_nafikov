package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the natural number");
        int n = scanner.nextInt();
        int result = reverse(n);
        System.out.println(result);
    }

    public static int reverse(int n) {
        if (n < 10) {
            return n;
        }
        return n % 10 * (int) Math.pow(10, (int) Math.log10(n)) + reverse(n / 10);
    }
}
