package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh03N029F.isTrue;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh03N029FTest {
    public static void main(String[] args) {
        testIsTrue1();
        testIsTrue2();
        testIsTrue3();
        testIsTrue4();
    }

    public static void testIsTrue1() {
        assertEquals("TaskCh03N029FTest.testIsTrue1", true, isTrue(105, 200, 300));
    }

    public static void testIsTrue2() {
        assertEquals("TaskCh03N029FTest.testIsTrue2", true, isTrue(0, 500, 50));
    }

    public static void testIsTrue3() {
        assertEquals("TaskCh03N029FTest.testIsTrue3", true, isTrue(120, 999, -5));
    }

    public static void testIsTrue4() {
        assertEquals("TaskCh03N029FTest.testIsTrue4", false, isTrue(50, 100, -5));
    }
}
