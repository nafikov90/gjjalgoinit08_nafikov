package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N047.getFiboN;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFibo1();
        testFibo2();
    }

    public static void testFibo1() {
        assertEquals("TaskCh10N047Test.testFibo1", 1, getFiboN(1));
    }

    public static void testFibo2() {
        assertEquals("TaskCh10N047Test.testFibo2", 5, getFiboN(5));
    }
}
