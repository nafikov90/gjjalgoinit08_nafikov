package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh10N048 {
    static final int ZERO_INDEX = 0;
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the amount of elements in the array");
        int arrayLength = scanner.nextInt();
        int maxEl = maxElemOfArray(createArray(arrayLength), ZERO_INDEX);
        System.out.println("Maximum element of the array: " + maxEl);
    }

    public static int maxElemOfArray(int[] input, int index) {
        if (index == input.length - 1) {
            return input[index];
        }
        int max = maxElemOfArray(input, index + 1);
        return input[index] < max ? max : input[index];
    }

    public static int[] createArray(int n) {
        int[] input = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Enter " + i + " element of the array");
            input[i] = scanner.nextInt();
        }
        return input;
    }
}
