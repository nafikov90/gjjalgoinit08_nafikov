package com.getjavajob.training.algo.init.nafikovi;

public class TaskCh10N053 {
    static final int ZERO_INDEX = 0;

    public static void main(String[] args) {
        int[] arrayBeforeReverse = {1, 2, 3, 4, 5};
        int[] result = reverseArray(arrayBeforeReverse, ZERO_INDEX, arrayBeforeReverse.length - 1);
        for (int a : result) {
            System.out.print(a + " ");
        }
    }

    public static int[] reverseArray(int[] input, int left, int right) {
        if (left < right) {
            int tmp = input[left];
            input[left] = input[right];
            input[right] = tmp;
            reverseArray(input, left + 1, right - 1);
        }
        return input;
    }
}
