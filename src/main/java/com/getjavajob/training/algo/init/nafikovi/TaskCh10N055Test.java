package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh10N055.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        test16x();
        test8x();
        test4x();
        test2x();
    }

    public static void test16x() {
        assertEquals("TaskCh10N055Test.test16x", 64, convertToAnotherSystem(100, 16));
    }

    public static void test8x() {
        assertEquals("TaskCh10N055Test.test8x", 101, convertToAnotherSystem(65, 8));
    }

    public static void test4x() {
        assertEquals("TaskCh10N055Test.test4x", 121, convertToAnotherSystem(25, 4));
    }

    public static void test2x() {
        assertEquals("TaskCh10N055Test.test2x", 1000, convertToAnotherSystem(8, 2));
    }
}