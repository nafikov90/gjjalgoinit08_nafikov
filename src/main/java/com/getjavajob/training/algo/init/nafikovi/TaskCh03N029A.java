package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh03N029A {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number");
        int a = scanner.nextInt();
        System.out.println("Enter the number");
        int b = scanner.nextInt();
        boolean result = isTrue(a, b);
        System.out.println(result);
    }

    public static boolean isTrue(int a, int b) {//return true if both numbers are odd
        return Math.abs(a) % 2 == 1 && Math.abs(b) % 2 == 1;
    }
}
