package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number k (1 <= k <=365)");
        int k = scanner.nextInt();
        String result = isWeekend(k);
        System.out.println(k + " day of the year is " + result);
    }

    public static String isWeekend(int k) {
        int a = k % 7;
        return a == 0 || a == 6 ? "Weekend" : "Workday";
    }
}
