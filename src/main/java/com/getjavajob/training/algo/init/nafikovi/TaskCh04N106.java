package com.getjavajob.training.algo.init.nafikovi;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of month");
        int numberOfMonth = scanner.nextInt();
        String result = whatIsSeason(numberOfMonth);
        System.out.println(result);
    }

    public static String whatIsSeason(int numberOfMonth) {
        switch (numberOfMonth % 12 / 3) {
            case 0:
                return "Winter";
            case 1:
                return "Spring";
            case 2:
                return "Summer";
            case 3:
                return "Autumn";
        }
        return "Input Error";
    }
}
