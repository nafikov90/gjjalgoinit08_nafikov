package com.getjavajob.training.algo.init.nafikovi;

import static com.getjavajob.training.algo.init.nafikovi.TaskCh04N106.whatIsSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        seasonTest1();
        seasonTest2();
        seasonTest3();
        seasonTest4();
        seasonTest5();
        seasonTest6();
        seasonTest7();
        seasonTest8();
        seasonTest9();
        seasonTest10();
        seasonTest11();
        seasonTest12();
    }

    public static void seasonTest1() {
        assertEquals("TaskCh04N106Test.seasonTest1", "Winter", whatIsSeason(1));
    }

    public static void seasonTest2() {
        assertEquals("TaskCh04N106Test.seasonTest2", "Winter", whatIsSeason(2));
    }

    public static void seasonTest3() {
        assertEquals("TaskCh04N106Test.seasonTest3", "Spring", whatIsSeason(3));
    }

    public static void seasonTest4() {
        assertEquals("TaskCh04N106Test.seasonTest4", "Spring", whatIsSeason(4));
    }

    public static void seasonTest5() {
        assertEquals("TaskCh04N106Test.seasonTest5", "Spring", whatIsSeason(5));
    }

    public static void seasonTest6() {
        assertEquals("TaskCh04N106Test.seasonTest6", "Summer", whatIsSeason(6));
    }

    public static void seasonTest7() {
        assertEquals("TaskCh04N106Test.seasonTest7", "Summer", whatIsSeason(7));
    }

    public static void seasonTest8() {
        assertEquals("TaskCh04N106Test.seasonTest8", "Summer", whatIsSeason(8));
    }

    public static void seasonTest9() {
        assertEquals("TaskCh04N106Test.seasonTest9", "Autumn", whatIsSeason(9));
    }

    public static void seasonTest10() {
        assertEquals("TaskCh04N106Test.seasonTest10", "Autumn", whatIsSeason(10));
    }

    public static void seasonTest11() {
        assertEquals("TaskCh04N106Test.seasonTest11", "Autumn", whatIsSeason(11));
    }

    public static void seasonTest12() {
        assertEquals("TaskCh04N106Test.seasonTest12", "Winter", whatIsSeason(12));
    }
}
